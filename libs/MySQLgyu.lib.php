<?php

### GYURAL ###

/*

----------
Gyural 1.8
----------

Filename: /libs/MySQLgyu.lib.php
 Version: 1.8
  Author: federicoq <f.quagliotto@mandarinoadv.com>
    Date: 10/10/13

*/

class MySQLgyu extends mysqli {
	
	public function Query($query) {
		
		$qUID = uniqid();
		## Caution.. it won't work with UNIONS.
		if(stristr($query, "SELECT") && !stristr($query, "UNION")) {
			preg_match("/\s+from\s+`?([a-z\d_]+)`?/i", $query, $match);
			$tableName = $match[1];
		}
		
		if(!isset($GLOBALS["num_query"]))
			$GLOBALS["num_query"] = 0;
		
		$GLOBALS["num_query"]++;
		$this->real_query($query);
		$a = new MySQLgyu_result($this);

		if(dev)
			if(strlen($this->error) > 0)
				die('<strong>GYU_ERROR</strong> <em>(database)</em> ' . $this->error . '<br />' . $query);
		
		isset($tableName) ? $a->{excludeFieldPrefix . 'table'} = str_replace(tablesPrefix, '', $tableName) : ''; # Retrive table name from the $query. note up.
		return($a);
		
	}

}

class MySQLgyu_result extends mysqli_result {
	
	public function oggettizza($object, $args, $query) {
		
		if($args)
			$a = $this->fetch_object($object, $args);
		else
			$a = $this->fetch_object($object);
		
		if($a) {

			#isset($this->gyu_table) ? $a->{excludeFieldPrefix . 'table'} = $this->{excludeFieldPrefix . 'table'} : ''; # as above.
				
			if(method_exists($a, 'gyu_tablesPrefix'))
				$a->gyu_tablesPrefix();

			if(dev) $a->gyu_query_performed = $query;

			return($a);
		}
		
	}
	
}

?>