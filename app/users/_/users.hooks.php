<?php

CallFunction('hooks', 'set', 'gyu.db.ready', 'users_hook__login');
CallFunction('hooks', 'set', 'gyu.after-libs', 'users_hook__init');

function users_hook__init($time = null) {
	LoadClass('users');
}

function users_hook__login($time = null) {
	
	$tried = false;
	
	if(isset($_POST["username"], $_POST["password"])) {
			
		$username = $_POST["username"];
		$password = $_POST["password"];

		if($user = LoadClass('users', 1)->_try($username, $password))
			$_SESSION["login"] = $user;
		else
			unset($_SESSION["login"]);

		$tried = true;

	} else if(@is_object($_SESSION["login"])) {
		
		if(!$_SESSION["login"]->_refresh())
			unset($_SESSION["login"]);

		$tried = true;

	}
	
	if($tried && !logged) {
		$wrongUrl = $_POST["uriLogin"] ? $_POST["uriLogin"] : uriLogin;
		header('Location: '.$wrongUrl);
		return false;
	}

	if(logged())
		Me()->_ping();

}

function users_hook__test($time) {
	echo 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';
}

?>